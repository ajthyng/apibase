import { Controller, Get, Req, Res } from '@nestjs/common'
import { Request, Response } from 'express'
import { CasService } from './cas.service'
import { AuthnService } from 'src/authn/authn.service'

@Controller('cas')
export class CasController {
  constructor (
    private readonly casService: CasService,
    private readonly authnService: AuthnService
  ) {}

  @Get('/login')
  async login (@Req() req: Request, @Res() res: Response) {
    const isAuthenticated = await this.authnService.isAuthenticated(req)
    if (isAuthenticated) {
      return res.redirect('/')
    }

    try {
      const token = await this.casService.validateTicket(req)
      return this.authnService.setCookie({ res, token }).redirect('/')
    } catch (err) {
      console.log('Login Error: ', err)
      return this.authnService.clearCookie({ res }).send({ error: err.message })
    }
  }
}
