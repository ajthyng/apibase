import { Module } from '@nestjs/common'
import { CasModule } from './cas/cas.module'

@Module({
  imports: [CasModule],
  controllers: [],
  providers: []
})

export class AppModule {}
