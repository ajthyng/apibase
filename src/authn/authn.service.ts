import { Injectable } from '@nestjs/common'
import { readFileSync } from 'fs'
import { Algorithm, verify, sign } from 'jsonwebtoken'
import { JWTPayload } from './jwt.dto'
import { Request } from 'express'
import { isEmpty } from 'lodash'
import { SetCookieDto, ClearCookieDto } from 'src/cas/cookie.dto'

interface JWTContents {
  user: string
  originalUser?: string
  app: string
  iat: number
  exp: number
}

@Injectable()
export class AuthnService {
  private readonly privateKey = readFileSync('/usr/app/jwt/private.pem')
  private readonly publicKey = readFileSync('/usr/app/jwt/public.pem')
  private readonly appName = 'apibase'
  private readonly algorithm: Algorithm = 'RS256'
  private readonly expiration = '30d'
  private readonly _cookieName = '_token'

  private _verify (token: string): JWTContents {
    return verify(token, this.publicKey, { algorithms: [this.algorithm] }) as JWTContents
  }

  private _sign (payload: JWTPayload) {
    return sign(payload, this.privateKey, { algorithm: this.algorithm, expiresIn: this.expiration })
  }

  public async isAuthenticated (req: Request) {
    const token = req.cookies[this._cookieName]
    if (!token) return false

    const contents = this._verify(token)
    return contents.app === this.appName && !isEmpty(contents.user)
  }

  public async createToken (user: string) {
    return this._sign({ user, app: this.appName })
  }

  public setCookie ({ res, token, options }: SetCookieDto) {
    return res.cookie(this._cookieName, token, options)
  }

  public clearCookie ({ res }: ClearCookieDto) {
    return res.cookie(this._cookieName, null, {
      expires: new Date()
    })
  }

  public become (token: string, user: string) {
    const contents = this._verify(token)

    const originalUser = contents.originalUser ?? contents.user
    const impersonatedUser = user

    const app = contents.app

    return this._sign({
      user: impersonatedUser,
      app,
      originalUser
    })
  }
}
