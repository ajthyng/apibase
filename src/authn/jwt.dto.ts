export interface JWTPayload {
  user: string
  app: string
  originalUser?: string
}
